const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProfileSchema = new Schema({
    profile: {
        type: String,
        required: 'Введите кодовое название профиля'
    },
    title: {
        type: String,
        required: 'Введите название профиля'
    },
    description: String
});

const Profile = mongoose.model('Profile', ProfileSchema);

module.exports = Profile;