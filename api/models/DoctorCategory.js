import mongoose from 'mongoose';

async function isExist(value) {
  const doctorCategory = await DoctorCategory.findOne({doctorCategory: value});
  return !doctorCategory;
}

const validators = [
  {
    validator: isExist,
    message: 'Такая категория существует!'
  }
];

const Schema = mongoose.Schema;

const DoctorCategorySchema = new Schema({
  doctorCategory: {
    validate: validators,
    type: String,
    required: 'Обязательное поле!',
  },
  isDeleted: {
    type: Boolean,
    default: false
  }
});

const DoctorCategory = mongoose.model('DoctorCategory', DoctorCategorySchema);

module.exports = DoctorCategory;