const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RecordSchema = new Schema({
    patientId: {
        type: Schema.Types.ObjectId,
        ref: 'PatientProfile',
        required: true
    },
    doctorId: {
        type: Schema.Types.ObjectId,
        ref: 'DoctorProfile',
        required: true
    },
    patientConfirm: {
        type: Boolean,
        default: false
    },
    doctorConfirm: {
        type: Boolean,
        default: false
    },
    status: {
        type: String,
        enum: ["new", "approved", "declined", "rescheduled"],
        default: "new"
    },
    date: {
        type: String
    },
    time: {
        type: String
    },
    complaint: {
        type: String,
        required: "Введите жалобу"
    }
});

const Record = mongoose.model('Record', RecordSchema);

module.exports = Record;