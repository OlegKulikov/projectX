const mongoose = require('mongoose');

const Schema = mongoose.Schema;

async function isExistDiploma(value) {
    if (!this.isModified('serialDiploma')) return;
    const doc = await DoctorProfile.findOne({serialDiploma: value});
    return !doc;
}

const DoctorProfileSchema = new Schema({
    name: {
        type: String,
        required: 'Введите имя',
        immutable: true
    },
    surname: {
        type: String,
        required: 'Введите фамилию',
        immutable: true
    },
    thirdname: {
        type: String,
        required: 'Введите отчество',
        immutable: true
    },
    passportId: {
        type: String,
        required: 'Введите айди паспорта',
        immutable: true
    },
    passportNumber: {
        type: String,
        required: 'Введите номер паспорта',
        immutable: true
    },
    phone: {
        type: String,
        required: 'Введите номер телефона'
    },
    dateOfBirth: {
        type: Date,
        required: 'Введите дату рождения',
        immutable: true
    },
    DateOfStartWorking: {
        type: Date,
        required: 'Введите дату начала работы',
        immutable: true
    },
    serialDiploma: {
        type: String,
        validate: {
            validator: isExistDiploma,
            message: 'Пользователь с таким дипломом уже зарегистрирован'
        },
        required: 'Введите серию диплома',
        immutable: true
    },
    clinic: [{
        type: Schema.Types.ObjectId,
        ref: 'ClinicProfile',
    }],
    workPlaces: [{
        type: Schema.Types.ObjectId,
        ref: 'WorkPlaces', 
    }],
    scheduleWork: {
        type: [Number],
        required: 'Введите режим работы'
    },
    gender: {
        type: String,
        required: 'Введите пол',
        enum: ['male', 'female'],
        immutable: true
    },
    doctorCategory: [{
        type: Schema.Types.ObjectId,
        ref: 'DoctorCategory',
        required: 'Укажите Вашу специальность'
    }],
    additionalCategory: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        unique: true,
        immutable: true
    },
    approved: {
        type: Boolean,
        default: false,
        immutable: true
    },
    deactivate: {
        type: Boolean,
        default: false,
        immutable: true
    },
    patients: [{
        type: Schema.Types.ObjectId,
        ref: 'PatientProfile'
    }],
    rating: [{
        patient: {
            type: Schema.Types.ObjectId,
            ref: 'PatientProfile'
        },
        rating: Number,
        review: String,
        datetime: Date,
        anonymous: Boolean
    }]
});

DoctorProfileSchema.set('toJSON', {
    transform: (doc, ret) => {
        delete ret._id;
        delete ret.approved;
        delete ret.deactivate;
        return ret
    }
});

const DoctorProfile = mongoose.model('DoctorProfile', DoctorProfileSchema);

module.exports = DoctorProfile;
