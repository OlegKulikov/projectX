import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import nanoid from 'nanoid';
import validator from "email-validator";

const SALT_WORK_FACTOR = 10;

async function isExist(value) {
  if (!this.isModified('email')) return;
  const user = await User.findOne({email: value});
  return !user;
}

async function validateMail(value) {
  if (!this.isModified('email')) return;
  return validator.validate(value);
}

const validators = [
  {
    validator: isExist,
    message: 'Эта почта уже зарегистрирована'
  },
  {
    validator: validateMail,
    message: 'Адрес почты не валидный'
  }
];

const Schema = mongoose.Schema;

const UsersSchema = new Schema({
  email: {
    validate: validators,
    type: String,
    required: 'Введите электронный адрес',
  },
  password: {
    type: String,
    required: 'Введите пароль'
  },
  permissions: [{
    type: String,
    enum: ['admin', 'patient', 'doctor', 'shop', 'clinic', 'superAdmin'],
    default: 'patient'
  }],
  token: {
    type: String,
    required: true
  },
  avatar: String,
  preview: String
});

UsersSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  const hash = await bcrypt.hash(this.password, salt);

  this.password = hash;

  next();
});

UsersSchema.methods.checkPassword = function (password) {
  return bcrypt.compare(password, this.password)
};

UsersSchema.methods.generateToken = function () {
  this.token = nanoid();
};

UsersSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.password;
    delete ret._id;
    return ret
  }
});

const User = mongoose.model('User', UsersSchema);

export default User;
