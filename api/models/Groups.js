import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import nanoid from 'nanoid';

const SALT_WORK_FACTOR = 10;

const Schema = mongoose.Schema;

const UsersSchema = new Schema({
    email: {
        validate: {
            validator: async function (value) {
                if (!this.isModified('email')) return;

                const user = await User.findOne({email: value});

                if (user) throw new Error();
            },
            message: 'Эта почта уже зарегистрирована'
        },
        type: String,
        unique: true,
        required: 'Введите электронный адрес',
    },
    password: {
        type: String,
        required: 'Введите пароль'
    },
    profile: {
        type: Schema.Types.ObjectId,
        ref: 'Profile',
        required: true
    },
    role: {
        type: String,
        enum: ['user', 'admin'],
        default: 'user'
    },
    image: String,
    token: {
        type: String,
        required: true
    }
});

UsersSchema.pre('save', async function (next) {
    if (!this.isModified('password')) return next();

    const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    const hash = await bcrypt.hash(this.password, salt);

    this.password = hash;

    next();
});

UsersSchema.methods.checkPassword = function (password) {
    return bcrypt.compare(password, this.password)
};

UsersSchema.methods.generateToken = function () {
    this.token = nanoid();
};

UsersSchema.set('toJSON', {
    transform: (doc, ret, options) => {
        delete ret.password;
        return ret
    }
});

const User = mongoose.model('User', UsersSchema);

module.exports = User;
