const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const QuestionnaireSchema = new Schema({
    patientId: {
        type: Schema.Types.ObjectId,
        ref: 'PatientProfile',
        required: true
    },
    doctorId: {
        type: Schema.Types.ObjectId,
        ref: 'DoctorProfile',
        required: true
    },
    questionnaire: {
        type: [String]
    }
});

const Questionnaire = mongoose.model('Questionnaire', QuestionnaireSchema);

module.exports = Questionnaire;