const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PatientProfileSchema = new Schema({
  name: {
    type: String,
    required: 'Введите имя',
    immutable: true
  },
  surname: {
    type: String,
    required: 'Введите фамилию',
    immutable: true
  },
  thirdname: {
    type: String,
    required: 'Введите отчество',
    immutable: true
  },
  dateOfBirth: {
    type: Date,
    required: 'Введите дату рождения',
    immutable: true
  },
  gender: {
    type: String,
    required: 'Введите пол',
    enum: ['male', 'female'],
    immutable: true
  },
  phone: {
    type: String,
    required: 'Введите номер телефона'
  },
  workAddress: {
    type: String,
    required: 'Введите рабочий адрес'
  },
  residence: {
    type: String,
    required: 'Введите место жительства'
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    unique: true
  },
  notes: [],
});

const PatientProfile = mongoose.model('PatientProfile', PatientProfileSchema);

module.exports = PatientProfile;
