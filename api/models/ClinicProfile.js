const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ClinicProfileSchema = new Schema({
    title: {
        type: String,
        required: 'Введите название клиники'
    },
    description: {
        type: String,
        required: 'Введите описание клиники'
    },
    address: {
        type: String,
        required: 'Укажите адрес клиники'
    },
    approved: {
        type: String,
        enum: ['approved', "pending", "declined"],
        default: "pending"
    },
    phone: {
        type: String,
        required: 'Введите номер телефона'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        unique: true
    }
});


const ClinicProfile = mongoose.model('ClinicProfile', ClinicProfileSchema);

module.exports = ClinicProfile;
