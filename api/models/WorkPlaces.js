const mongoose = require('mongoose');

const Schema = mongoose.Schema;

async function isExist(value) {
    if (!this.isModified('serialDiploma')) return;
    const { location: { coordinates } } = value;
    const options = { near: coordinates, maxDistance: 5 };
   const workPlace =  await WorkPlace.geoSearch({ type : 'Clinic' }, options);
   return !workPlace;
}

const WorkPlaceShema = new Schema({
    name: String,
    validate: {
        validator: isExist,
        message: 'Такое место уже существует, выбирете из списка'
    },
    location: {
        type: {
            type: String,
            enum: ['Clinic'],
            required: true
        },
        coordinates: {
            type: [Number],
            required: true
        }
    }
});

const WorkPlace = mongoose.model('WorkPlace', WorkPlaceShema);

module.exports = WorkPlace;
