import path from 'path';
const rootPath = __dirname;

export default {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    userImages: 'uploads/users',
    dbUrl : 'mongodb://localhost/SDoctorom',
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    },
    mailUser: 'projetp901@gmail.com',
    mailPass: 'QwErTy123'
};

