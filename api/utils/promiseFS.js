import fs from "fs";

export const copyFile = (currentPath, targetPath) =>  new Promise((resolve, reject) => {
    fs.copyFile(currentPath, targetPath, async (error) => {
        if (error) reject(error);
        resolve();
    });
});

export const removeFile = (path) =>  new Promise((resolve, reject) => {
    fs.unlink(path, async (error) => {
        if (error) reject(error);
        resolve();
    });
});