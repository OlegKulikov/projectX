import axios from 'axios';
import qs from 'querystring';

export default async (data) => {
    const {
        name,
        surname,
        thirdname,
        serialDiploma,
    } = data;
    try {
        const options = {
            url: 'http://proverka.oaouchkun.kg/blogs/serch.php',
            method: 'post',
            headers: {
                Accept: 'application/x-www-form-urlencoded',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: qs.stringify({
                document: 'diploma',
                n_seria: serialDiploma,
                fio: `${surname.trim()} ${name.trim()} ${thirdname.trim()}`
            })
        };
        const result = await axios.request(options);
        return result.data.includes('запись имеется в базе зарегистрированных документов');
    } catch (e) {
        return undefined;
    }
}
