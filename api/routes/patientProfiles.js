import express from 'express';
import auth from '../middleware/auth';
import permit from '../middleware/permit';
import Patients from '../models/PatientProfile';

const router = express.Router();

/**
 * @api {post} /patientProfiles  добавление профиля пациента
 * @apiName post patientProfiles
 * @apiGroup Profile
 *
 * @apiDescription *Все параметры предаются в теле запроса.
 ** формат JSON.
 * @apiPermission user
 * @apiHeader (MyHeaderGroup) {String} Authorization Authorization value.
 * @apiParam {String} name Name of patient
 * @apiParam {String} surname Surname of patient
 * @apiParam {String} thirdname Thirdname of patient
 * @apiParam {String} dateOfBirth Date of birth patient
 * @apiParam {String} gender Gender of patient
 * @apiParam {String} phone Phone of patient
 * @apiParam {String} workAddress Work address of patient
 * @apiParam {String} residence Residence of patient
 *
 * @apiParamExample {JSON} Request-Example:
 *     {
 *          "name": "Василий",
 *          "surname": "Передёргин",
 *          "thirdname": "Васильевич",
 *          "dateOfBirth": "05.12.1956",
 *          "gender": "male",
 *          "phone": "+996555123456",
 *          "workAddress": "ул. Киевская, 165 кв.5",
 *          "residence": "пр. Мира, 36"
 *     }
 *
 * @apiSampleRequest /patientProfiles/add
 */

router.post('/add', auth, async (req, res) => {
  const {user, body} = req;
  body.user = user._id;
  try {
    const patient = await new Patients(body);
    await patient.save();
    return res.send({message: "Ваша анкета сохранена", patient});
  } catch (error) {
    return res.status(400).send({...error, message: "Убедитесь что все поля заполнины!"});
  }
});

////////////////////////////////////////////////////

/**
 * @api {put} /patientProfiles  Редактирование профиля пациента
 * @apiName Edit patientProfiles
 * @apiGroup Profile
 *
 * @apiDescription *Все параметры предаются в теле запроса.
 ** формат JSON.
 * @apiPermission user
 * @apiHeader (MyHeaderGroup) {String} Authorization Authorization value.
 *
 * @apiParam {String} phone Phone of patient
 * @apiParam {String} workAddress Work address of patient
 * @apiParam {String} residence Residence of patient
 *
 *@apiSuccessExample {json} Success-Response:
 *     {
          "message": "Анкета отредактирована",
          "patient": {
              "notes": [],
              "_id": "5e4ffc256471c817996936ec",
              "name": "Василий",
              "surname": "Передёргин",
              "thirdname": "Васильевич",
              "dateOfBirth": "1956-05-11T18:00:00.000Z",
              "gender": "male",
              "phone": "+996555123456",
              "workAddress": "ул. Киевская, 165 кв.5",
              "residence": "пр. Мира, 36",
              "user": "5e4d3981533aca3950081449",
              "__v": 0
          }
 *     }
 *
 * @apiSampleRequest /patientProfiles/edit
 */

router.put('/edit', [auth, permit('patient')], async (req, res) => {
  const {user, body} = req;
  try {
    let patient = await Patients.findByIdAndUpdate(user._id, body, null, (patient) => patient );
    return res.send({message: "Анкета отредактирована", patient});
  } catch (error) {
    return res.status(400).send({...error, message: "Убедитесь чтовсе поля заполнины!"});
  }
});

///////////////////////////////////////////////

/**
 * @api {get} /patientProfiles Запрос вех профилей пациентов
 * @apiName get patientProfiles
 * @apiGroup Profile
 *
 * @apiPermission admin, superAdmin
 *
 * @apiHeader (MyHeaderGroup) {String} Authorization Authorization value.
 *
 * @apiSampleRequest /patientProfiles/get
 */

router.get('/get', [auth, permit('superAdmin', 'admin')], async (req, res) => {
  try {
    const patients = await Patients.find();
    return res.send(patients);
  } catch (error) {
    return res.status(500).send({...error, message: "Что-то пошло не так!"});
  }
});

/////////////////////////////////////////////////////

module.exports = router;