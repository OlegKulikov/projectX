const express = require('express');

const Profile = require('../models/Profile');
const router = express.Router();

router.get('/', async (req, res) => {
        const profiles = await Profile.find();
        if (profiles) return res.send(profiles);
        return res.sendStatus(500);
});

module.exports = router;