import express from 'express';

import diplomaCheker from "../utils/diplomaCheker";
import auth from '../middleware/auth' ;
import permit from '../middleware/permit';
import DoctorProfile from '../models/DoctorProfile';

const router = express.Router();

/**
 * @api {post} /doctorProfiles/add добавление доктора
 * @apiName create doctor profile
 * @apiGroup Doctors
 *
 * @apiDescription *Все параметры предаются в теле запроса.
 ** формат JSON.
 * @apiPermission users
 * @apiHeader (MyHeaderGroup) {String} Authorization Authorization value.
 * @apiParam {String} name  name of user
 * @apiParam {String} surname  surname of user.
 * @apiParam {String} thirdname thirdname of user.
 * @apiParam {String} passportNumber passport number of user.
 * @apiParam {String} passportId id code from passport of user.
 * @apiParam {String} phone phone of user.
 * @apiParam {Date} dateOfBirth  user birthday.
 * @apiParam {Date} DateOfStartWorking Date Of StartWorking.
 * @apiParam {String} serialDiploma unique serial diploma
 * @apiParam {Array} [clinic] id of clinic.
 * @apiParam {Array} [workPlaces] id of work places.
 * @apiParam {Array} scheduleWork work days.
 * @apiParam {String} gender (male or female).
 * @apiParam {Number} doctorCategory id of doctor category.
 * @apiParam {String}  additionalCategory some additional category.
 * @apiParam {String} required password of user.
 *
 * @apiParamExample {JSON} Request-Example:
 *     {
          "name": "Олег",
          "surname": "Куликов",
           "thirdname": "Игоревич",
           "passportId": "123123123123",
           "passportNumber": "id23923",
           "phone": "777-320-693",
           "dateOfBirth": "Tue Feb 18 2020 15:44:33 GMT+0600 (Киргизия)",
           "DateOfStartWorking": "Tue Feb 18 2020 15:44:33 GMT+0600 (Киргизия)",
           "serialDiploma": "УВ150140141",
           "scheduleWork": "[1,2,3,4,5]",
           "gender": "male",
           "doctorCategory": "[5e5a2a4ef76523e8d75bde28]",
           "additionalCategory": "глазных дел мастер"
 *     }
 *
 * @apiSampleRequest /doctorProfiles/add
 */
router.post('/add', [auth, permit('doctor')], async (req, res) => {
    const { body, user: {_id} } = req;
    console.log(body);
    const validateDiploma = await diplomaCheker(body);
    if(!validateDiploma) return res.send({ message: 'Диплом не прошел проверку.' });
    try {
        const doctorProfile = await new DoctorProfile({...body, user: _id});
        await doctorProfile.save();
        return res.send({ message: 'Анкета сохранена', body: doctorProfile });
    } catch (error) {
        return res.status(400).send({...error,  message: 'Убедитесь что все поля заполнены'});
    }
});

/**
 * @api {put} /doctorProfiles/edit редактирование Доктора
 * @apiName Edit doctor
 * @apiGroup Doctor
 *
 * @apiDescription *Все параметры предаются в теле запроса.
 ** формат JSON.
 * @apiPermission Доктора
 * @apiHeader (MyHeaderGroup) {String} Authorization Authorization value.
 *
 * @apiParam {String} [phone] change number phone.
 * @apiParam {String} [additionalCategory] additional category.
 * @apiParam {Array} [scheduleWork] schedule work.
 * @apiParam {Array} [doctorCategory] doctor categories.
 *
 * @apiParamExample {JSON} Request-Example:
 *     {
           "phone": "777-320-693",
           "scheduleWork": [1,2,3,4,5],
           "doctorCategory": [5e5a2a4ef76523e8d75bde26, 5e5a2a4ef76523e8d75bde28],
           "additionalCategory": "какая то дополнительная категория"
 *     }
 *     
 * @apiSuccessExample {json} Success-Response:
 *     {
    "message": "Профиль отредактирован",
    "user": {
        "permissions": [
            "user"
        ],
        "email": "vasy23a@mail.ru",
        "token": "s1UO7oKffTE6ZzcLKtA44",
        "__v": 0,
        "avatar": "undefined5e381d9ab666e51340edd1ea/fullSize.jpg",
        "prevew": "undefined5e381d9ab666e51340edd1ea/prevew.jpg"
    }
}
 @apiSampleRequest /doctorProfiles/edit
 */
router.put('/edit', [auth, permit('doctor')], async (req, res) => {
    const { body, user: {_id} } = req;
    try {
        const doc = await DoctorProfile.findOneAndUpdate({user: _id},  body, (doc) => doc);
        return res.send({ message: 'Профиль отредактирован', doc });
    } catch (error) {
        return res.status(400).send({ message: 'Редактрование не удалось' });
    }
});

router.put('/:id', auth, async (req, res) => {
    const { user: { _id }, params: { id }, body } = req;
    let doctor = await DoctorProfile.findById(id);
    let rating = doctor.rating.find(rating => rating.patient.equals(_id));
    if (!rating) {
        doctor.rating.push({
            patient: _id,
            datetime: new Date().toISOString(),
            ...body
        });
        try{
            await doctor.save();
            return res.send({ doctor, message: "Рейтинг выставлен" });
        } catch (error) {
            return res.status(503).send({ message: "Что то пошло не так..." });
        }
    } else {
       return res.status(400).send({ message: "Оценить врача можно всего раз за лечение" });
    }
});

export default router;
