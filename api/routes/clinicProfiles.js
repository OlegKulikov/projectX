const express = require('express');
const auth = require('../middleware/auth');
const ClinicProfile = require('../models/ClinicProfile');
const axios = require('axios');
const config = require('../config');


const router = express.Router();

router.get('/', (req, res) => {
    let criteria = {approved: "approved"};
    let select = {
        title: "title",
        description: "description",
        address: "address",
        phone: "phone",
        user: "user"
    };

    if (req.query.user) {
        criteria = {
            user: req.query.user
        };
        select = "";
    }
    ClinicProfile.find(criteria).select(select).populate({
        path: "user",
        select: {
            image: "image"
        }
    }).select(select)
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

router.get('/:id', (req, res) => {
    ClinicProfile.findById(req.params.id).populate({
        path: "user",
        select: {
            image: "image"
        }
    })
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

router.post('/', auth, async (req, res) => {
    const data = req.body;
    data.user = req.user._id;
    try {
        const clinic = await new ClinicProfile(data);
        await clinic.save();
        const notification = {
            text: "Новый аккаунт Клиники отправлен на утверждение",
        };
        await axios.post(config.wsURL + '/admin/notifications', notification);
        return res.send({message: 'Анкета сохранена'});
    } catch (e) {
        return res.status(400).send(e);
    }
});


router.put('/', auth, async (req, res) => {
    let clinic = await ClinicProfile.findOne({user: req.user._id});
    if (clinic.user.equals(req.user._id)) {
        if (req.body.title) {
            clinic.title = req.body.title;
        }
        if (req.body.description) {
            clinic.description = req.body.description;
        }
        if (req.body.address) {
            clinic.address = req.body.address;
        }
        if (req.body.phone) {
            clinic.phone = req.body.phone;
        }
        clinic.save()
            .then(result => res.send({result, message: 'Анкета изменена'}))
            .catch(error => res.status(400).send(error))
    } else {
        res.sendStatus(403);
    }
});


module.exports = router;