import express from 'express';
import formData from "express-form-data";
import mkdir from "create-directories";
import sharp from "sharp";

import User from '../models/User';
import auth from '../middleware/auth';
import config from "../config";
import {copyFile, removeFile} from "../utils/promiseFS";

const { uploadPath, userImages } = config;
const router = express.Router();

const options = {
    uploadDir: uploadPath + '/users'
};

router.use(formData.parse(options));
router.use(formData.union());

/**
 * @api {post} /users/sign? регистрация юзера
 * @apiName post user
 * @apiGroup User
 *
 * @apiDescription *Все параметры предаются в теле запроса.
 ** формат JSON.
 * @apiPermission Все
 * @apiParam {String} type type of user
 * @apiParam {String} mail mail of user
 * @apiParam {String} password password of user.
 *
 * @apiParamExample {JSON} Request-Example:
 *     {
 *          "email": "vasya@mail.ru",
 *          "password": "1234"
 *     }
 *
 * @apiSampleRequest /users/sign?type=usertype
*/
router.post('/sign?', async (req, res) => {
    const { email, password } = req.body;
    const { type } = req.query;
    let permit;
    switch (type) {
        case 'doctor':
        case 'shop':
        case 'clinic':
        permit = type;
        break;
        default: 
            permit = 'patient';
            break;
    }
    const user = await new User(
        {
            email,
            password
        }
    );
    user.generateToken();
    user.permissions.push(permit);

    try {
        await user.save();
        return res.send({ message: 'Регистрация прошла успешно', user });
    } catch (error) {
        return res.status(400).send({...error,  message: 'убедитесь что все поля заполненны'});
    }
});

/**
 * @api {get} /users/getProfile получение юзера
 * @apiName Get user
 * @apiGroup User
 * @apiPermission Все авторизованные пользователи
 * @apiHeader (MyHeaderGroup) {String} Authorization Authorization value.
 */

router.get('/getProfile', auth, (req, res) => res.send(req.user));

/**
 * @api {put} /users редактирование юзера
 * @apiName Edit user
 * @apiGroup User
 *
 * @apiDescription *Все параметры предаются в теле запроса.
 ** формат multipart/form-data.
 * @apiPermission Все авторизованные пользователи
 * @apiHeader (MyHeaderGroup) {String} Authorization Authorization value.
 * @apiHeader (MyHeaderGroup) {String} Content-Type multipart/form-data.
 *
 * @apiParam {String} [password] old password password of user.
 * @apiParam {Object} [avatar] avatar of user.
 *
 * @apiSuccessExample {json} Success-Response:
 *     {
    "message": "Профиль отредактирован",
    "user": {
        "permissions": [
            "user"
        ],
        "email": "vasy23a@mail.ru",
        "token": "s1UO7oKffTE6ZzcLKtA44",
        "__v": 0,
        "avatar": "undefined5e381d9ab666e51340edd1ea/fullSize.jpg",
        "prevew": "undefined5e381d9ab666e51340edd1ea/prevew.jpg"
    }
}
 @apiSampleRequest off
 */

router.put('/', auth, async (req, res) => {
    const { user,  body: { password, avatar } } = req;
    if(password) user.password = password;
    if (avatar && avatar.type.includes('jpeg')) {
        try {
            const type = avatar.path.replace(/.+[.]/, '');
            const originalName = `/${user._id}/fullSize.${type}`;
            const prevewName = `/${user._id}/prevew.${type}`;
            const originalPath = options.uploadDir + originalName;
            const thumbPath = options.uploadDir + prevewName;
            // TODO: переписать функцию mkdir на использование fs
            mkdir({[user._id]: {}}, options.uploadDir);
            await copyFile(avatar.path, originalPath);
            await sharp(originalPath).resize(400, 600).toFile(thumbPath);
            await removeFile(avatar.path);
            user.avatar = userImages + originalName;
            user.prevew = userImages + prevewName;
        } catch (e) {
            return res.status(400).send({message: 'загрузка изображения не удалась'})
        }
    }
    try {
        await user.save();
       return res.send({ message: 'Профиль отредактирован', user });
    } catch {
        return res.status(400).send({ message: 'Редактрование не удалось' });
    }
});

//////////////////////////////////////////////////////////////////
/**
 * @api {post} /users/sessions логин юзера
 * @apiName Log in user
 * @apiGroup User
 *
 * @apiDescription *Все параметры предаются в теле запроса.
 ** формат JSON.
 * @apiPermission Все
 * @apiParam {String} mail mail of user
 * @apiParam {String} password password of user.
 *
 * @apiParamExample {JSON} Request-Example:
 *     {
 *          "email": "vasya@mail.ru",
 *          "password": "1234"
 *     }
 *
 * @apiSampleRequest /users/sessions
 */

router.post('/sessions', async (req, res) => {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
        return res.status(401).send({ error: 'Неверный логин или пароль!' });
    }

    const isMatch = await user.checkPassword(password);
    if (!isMatch) {
        return res.status(401).send({ error: 'Неверный логин или пароль!' });
    }

    user.generateToken();

    try {
        await user.save();
        return  res.send({ message: 'Вы вошли успешно', user });
    } catch (error) {
        return res.status(500).send({ message: 'Что то пошло не так...' })
    }
});

/**
 * @api {delete} /users/sessions логаут юзера
 * @apiName Log out user
 * @apiGroup User
 *
 * @apiDescription *Все параметры предаются в header.
 ** формат JSON.
 * @apiPermission Все
 * @apiHeader (MyHeaderGroup) {String} Authorization Authorization value.
 *
 * @apiSampleRequest /users/sessions
 */

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Вы вышли успешно!'};

    if (!token) {
        return res.send(success);
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.send(success);
    }
    try {
        user.generateToken();
        await user.save();
        return res.send(success);
    } catch (error) {
        return res.status(500).send({ message: 'Что то пошло не так...' })
    }
    
});


export default router;
