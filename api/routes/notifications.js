const express = require('express');
const auth = require('../middleware/auth');
const Notification = require('../models/Notification');
const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        let criteria = {};
        if (req.query.patient) {
            criteria = {
                patient: req.query.patient
            };
        }
        if (req.query.doctor) {
            criteria = {
                doctor: req.query.doctor
            };
        }
        const notifications = await Notification.find(criteria).sort({datetime: -1}).populate({
            path: "doctor patient",
            select: {name: 'name', surname: 'surname', thirdname: 'thirdname'}
        });
        return res.send(notifications);

    } catch (e) {
        return res.status(500).send(e);
    }
});

router.get('/:id', auth, (req, res) => {
    Notification.findById(req.params.id).populate({
        path: "doctor patient record",
        populate: {path: 'patientId doctorId'},
        select: {name: 'name', surname: 'surname', thirdname: 'thirdname', date: "date", time: "time"}
    })
        .then(notification => {
            if (notification) res.send(notification);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.put('/:id', auth, async (req, res) => {
    try {
        const notification = await Notification.findById(req.params.id);
        if (!notification) {
            return res.sendStatus(404);
        }
        if (req.body.patientStatus) {
            notification.patientStatus = req.body.patientStatus;
        }
        if(req.body.doctorStatus) {
            notification.doctorStatus = req.body.doctorStatus;
        }

        await notification.save();
        res.send(notification);
    } catch (e) {
        res.status(500).send(e)
    }
});

router.post('/', async (req, res) => {
    const notification = new Notification({...req.body, datetime: new Date().toISOString()});
    notification.save()
        .then(result => res.send({result, message: "Получено новое уведомление"}))
        .catch(error => res.status(400).send(error));
});


module.exports = router;
