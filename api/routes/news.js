const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const News = require('../models/News');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    News.find().sort('-datetime').limit('-10').populate('user', 'email')
        .then(news => res.send(news))
        .catch(() => res.sendStatus(500));
});


router.get('/:id', (req, res) => {
    News.findById(req.params.id).sort('-datetime').populate('user', 'email')
        .then(post => {
            if (post) res.send(post);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.delete('/:id', auth, async (req, res) => {
    try{
        let news = await News.findById(req.params.id);
        if(news.user.equals(req.user._id)){
            News.deleteOne({_id: req.params.id}).then(
                result => res.send("Deleted successfully")
            )
        } else {
            res.sendStatus(403)
        }
    } catch (e) {
        res.sendStatus(500)
    }

});

router.post('/', [auth, upload.single('image')], async (req, res) => {


    if (req.body.description !== '' || req.body.title !== '' || req.body.image !== '') {

        const newsData = {...req.body, user: req.user._id, datetime: new Date().toISOString()};

        if (req.file) {
            newsData.image = req.file.filename;
        }

        const news = new News(newsData);

        news.save()
            .then(result => res.send({result, message: "Ваша новость опубликована"}))
            .catch(error => res.status(400).send(error));
    } else {
        return res.sendStatus(400);
    }
});


module.exports = router;
