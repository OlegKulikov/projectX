const express = require('express');
const PatientProfile = require('../models/PatientProfile');
const MedicalCard = require('../models/MedicalCard')

const auth = require('../middleware/auth');

const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        const cards = await MedicalCard.find({patientId: req.query.patient});
        if (cards.length) {
            return res.send(cards)
        } else {
            return res.send({message: 'Не найдено'})
        }
    } catch (e) {
        return res.status(400).send(e)
    }
});

router.get('/:id', auth, async (req, res) => {

    try {
        const result = await PatientProfile.findById(req.params.id);
        if (result) {
            return res.send(result)
        }
    } catch (e) {
        return res.status(400).send(e);
    }

});


router.post('/', auth, async (req, res) => {
    try {
        const data = req.body;
        data.patientId = req.query.patient;
        const medicalCard = await new MedicalCard(
            {
                doctorId: data.doctorId,
                patientId: req.query.patient,
                pastIllnesses: data.pastIllnesses,
                presentDisease: data.presentDisease,
                diagnosis: data.diagnosis,
                date: data.date
            }
        );
        medicalCard.save();
        return res.send({message: 'Данные успешно сохранены'})
    } catch (e) {
        return res.status(400).send(e);
    }
});


router.get('/view/:id', auth, async (req, res) => {
    try {
        const card = await MedicalCard.findById(req.params.id).populate({
            path: 'doctorId',
            select: {name: "name", surname: "surname", thirdname: "thirdname"}
        });

        return res.send(card)
    } catch (e) {
        return res.status(400).send(e)
    }
});

module.exports = router;
