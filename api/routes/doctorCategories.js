import express from 'express';

import auth from '../middleware/auth';
import permit from '../middleware/permit';
import DoctorCategory from '../models/DoctorCategory';


const router = express.Router();

/**
 * @api {post} /doctorCategories  добавление категории врача
 * @apiName post category
 * @apiGroup Category
 *
 * @apiDescription *Все параметры предаются в теле запроса.
 ** формат JSON.
 * @apiPermission admin, superAdmin
 * @apiHeader (MyHeaderGroup) {String} Authorization Authorization value.
 * @apiParam {String} doctorCategory Doctor category
 *
 * @apiParamExample {JSON} Request-Example:
 *     {
 *          "doctorCategory": "Мамолог"
 *     }
 *
 * @apiSampleRequest /doctorCategories
 */

router.post('/', [auth, permit('superAdmin', 'admin')], async (req, res) => {
  const {doctorCategory} = req.body;
  try {
    const categories = await new DoctorCategory({doctorCategory});
    await categories.save();
    return res.send({message: 'Категория создана упешно', categories});
  } catch (error) {
    return res.status(400).send(error);
  }
});

//////////////////////////////////////////

/**
 * @api {get} /doctorCategories Запрос вех категорий
 * @apiName get category
 * @apiGroup Category
 *
 * @apiPermission Все
 * @apiParam {String} doctorCategory Doctor category
 * @apiSampleRequest /doctorCategories
 */
router.get('/', async (req, res) => {
  try {
    const categories = await DoctorCategory.find({isDeleted: false});
    return res.send(categories);
  } catch (error) {
    return res.sendStatus(500);
  }
});

//////////////////////////////////////////

// router.get('/:id', async (req, res) => {
//   try {
//     const DoctorCategory = await DoctorCategory.findById(req.params.id);
//
//     return res.send(DoctorCategory);
//   } catch (e) {
//     return res.sendStatus(500);
//   }
// });


module.exports = router;