const express = require('express');
const auth = require('../middleware/auth');
const ShopProfiles = require('../models/ShopProfile');


const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        let shop = await ShopProfiles.findOne({user: req.user._id});

        if (shop.user.equals(req.user._id)) {
            res.send(shop);
        }

    } catch (e) {
        res.sendStatus(403);
    }
});


router.post('/', auth, async (req, res) => {
    const data = req.body;
    data.user = req.user._id;

    try {
        const shop = await new ShopProfiles(data);
        await shop.save();
        return res.send({message: 'Анкета сохранена'});
    } catch (e) {
        return res.status(400).send(e);
    }
});


router.put('/', auth, async (req, res) => {
    let shop = await ShopProfiles.findOne({user: req.user._id});
    if (shop.user.equals(req.user._id)) {
        if (req.body.title) {
            shop.title = req.body.title;
        }
        if (req.body.description) {
            shop.description = req.body.description;
        }
        if (req.body.address) {
            shop.address = req.body.address;
        }
        if (req.body.phone) {
            shop.phone = req.body.phone;
        }
        shop.save()
            .then(result => res.send({result, message: "Анкета изменена"}))
            .catch(error => res.status(400).send(error))
    } else {
        res.sendStatus(403);
    }

});
module.exports = router;