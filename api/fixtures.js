import mongoose from 'mongoose';
import nanoid from 'nanoid';
import config from './config';

import User from './models/User';
import DoctorCategory from './models/DoctorCategory';
// const ClinicProfile = require('./models/ClinicProfile');
// const Profile = require('./models/Profile');
// const DoctorProfile = require('./models/DoctorProfile');
// const PatientProfile = require('./models/PatientProfile');
// const Record = require('./models/Record');
// const ShopProfile = require('./models/ShopProfile');
// const News = require('./models/News');
// const Comment = require('./models/Comment');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const users = await User.create(
    {
      email: "superAdmin@mail.ru",
      password: '654321',
      permissions: ['superAdmin'],
      token: 'testToken',
    },
    {
      email: "vasya@mail.ru",
      password: '654321',
      permissions: ['patient'],
      token: 'testToken',
    },
    {
      email: "kulatovCool@mail.ru",
      password: '654321',
      permissions: ['doctor'],
      token: 'testToken',
    },
  );

  const doctorCategories = await DoctorCategory.create(
    {doctorCategory: "Клинический психолог"},
    {doctorCategory: "Микрохирург"},
    {doctorCategory: "Акушер гинеколог репродуктолог"},
    {doctorCategory: "Нутрицитолог"},
    {doctorCategory: "Хирург"},
    {doctorCategory: "Генетический консультант"},
    {doctorCategory: "Уролог"},
    {doctorCategory: "Детский хирург"},
    {doctorCategory: "Кардиолог"},
    {doctorCategory: "Иммунолог"},
    {doctorCategory: "Терапевт"},
    {doctorCategory: "Вирусолог"},
    {doctorCategory: "Акушер гинеколог"},
    {doctorCategory: "Фельдшер"},
    {doctorCategory: "Акушер"},
    {doctorCategory: "Гинеколог"},
    {doctorCategory: "Ветеринар классический"},
    {doctorCategory: "Ветеринар экзотический"},
    {doctorCategory: "Натуропат"},
    {doctorCategory: "Фармаколог"},
    {doctorCategory: "Гомеопат"},
    {doctorCategory: "Хирург эндокринолог"},
    {doctorCategory: "Хирург колопроктолог"},
    {doctorCategory: "Хирург ортопед"},
    {doctorCategory: "Хирург офтальмолог"},
    {doctorCategory: "Хирург травматолог"},
    {doctorCategory: "Хирург оториноларинголог"},
    {doctorCategory: "Андролог хирург"},
    {doctorCategory: "Санитар"},
    {doctorCategory: "Абдоминальный хирург"},
    {doctorCategory: "Нефролог"},
    {doctorCategory: "Хирург гинеколог"},
    {doctorCategory: "Хирург онколог"},
    {doctorCategory: "Хирург уролог"},
    {doctorCategory: "Пластический хирург"},
    {doctorCategory: "Разработчик нейропротезов"},
    {doctorCategory: "Тканевой инженер"},
    {doctorCategory: "Специалист по безопасности лекарственных средств"},
    {doctorCategory: "Валеолог"},
    {doctorCategory: "Медицинская сестра/брат"},
    {doctorCategory: "Старшая медицинская сестра"},
    {doctorCategory: "Главная медицинская сестра больницы"},
    {doctorCategory: "Онколог"},
    {doctorCategory: "Эндокринолог"},
    {doctorCategory: "Оптометрист"},
    {doctorCategory: "Врач клинической лабораторной диагностики"},
    {doctorCategory: "Физиотерапевт"},
    {doctorCategory: "Аллерголог"},
    {doctorCategory: "Офтальмолог (окулист)"},
    {doctorCategory: "Дерматолог"},
    {doctorCategory: "Анестезиолог-реаниматолог"},
    {doctorCategory: "Гастроэнтеролог"},
    {doctorCategory: "Диетолог"},
    {doctorCategory: "Кардиохирург"},
    {doctorCategory: "Мануальный терапевт"},
    {doctorCategory: "Массажист"},
    {doctorCategory: "Рентгенолог"},
    {doctorCategory: "Гериатр (геронтолог)"},
    {doctorCategory: "Патопсихолог"},
    {doctorCategory: "Психотерапевт"},
    {doctorCategory: "Психиатр"},
    {doctorCategory: "Инфекционист"},
    {doctorCategory: "Невропатолог (невролог)"},
    {doctorCategory: "Ревматолог"},
    {doctorCategory: "Реабилитолог"},
    {doctorCategory: "Торакальный хирург"},
    {doctorCategory: "Флеболог"},
    {doctorCategory: "Нейрохирург"},
    {doctorCategory: "Лор (отоларинголог)"},
    {doctorCategory: "Травматолог (травматолог ортопед)"},
    {doctorCategory: "Фтизиатр"},
    {doctorCategory: "Пульмонолог"},
    {doctorCategory: "Косметолог"},
    {doctorCategory: "Косметолог эстетист"},
    {doctorCategory: "Дерматокосметолог"},
    {doctorCategory: "Онкодерматолог"},
    {doctorCategory: "Сомнолог"},
    {doctorCategory: "Сурдолог"},
    {doctorCategory: "Патологоанатом"},
    {doctorCategory: "Фониатр"},
    {doctorCategory: "Педиатр"},
    {doctorCategory: "Трихолог"},
    {doctorCategory: "Провизор (фармацевт)"},
    {doctorCategory: "Художник-глазопротезист"},
    {doctorCategory: "Стоматолог"},
    {doctorCategory: "Стоматолог эндодонтист"},
    {doctorCategory: "Стоматолог микроскопист"},
    {doctorCategory: "Стоматолог ортопед"},
    {doctorCategory: "Стоматолог имплантолог"},
    {doctorCategory: "Стоматолог терапевт"},
    {doctorCategory: "Стоматолог ортодонт"},
    {doctorCategory: "Стоматолог детский"},
    {doctorCategory: "Стоматолог гигиенист"},
    {doctorCategory: "Стоматолог хирург"},
    {doctorCategory: "Стоматолог гнатолог"},
    {doctorCategory: "Челюстно лицевой хирург"},
    {doctorCategory: "Зубной техник"},
    {doctorCategory: "Миколог"},
    {doctorCategory: "Сексолог"},
    {doctorCategory: "Подолог"},
  );

  /*const profiles = await Profile.create(
      {title: 'Доктор', codeName: 'doctor', description: 'Описание профиля доктора'},
      {title: 'Пациент', codeName: 'patient', description: 'Описание профиля пациента'},
      {title: 'Магазин', codeName: 'shop', description: 'Описание профиля магазина'},
      {title: 'Клиника', codeName: 'clinic', description: 'Описание профиля клиники'},
  );

  const shops = await ShopProfile.create(
      {
          title: "Ананайка кидс",
          description: "Семейный магазин",
          address: "г.Бишкек, ул. Ибраимова, 84",
          phone: "+996 555–43–97–57",
          user: users[12],
          approved: "approved"
      }
  );

  const clinics = await ClinicProfile.create(
      {
          title: "Кроха",
          description: "Семейная клиника",
          address: "г.Бишкек, ул. Ибраимова, 84",
          phone: "+996 555–43–97–57",
          user: users[10],
          approved: "approved"
      },
      {
          title: "Неомед",
          description: "Клиника",
          address: "г. Бишкек, ул. Орозбекова 46",
          phone: "+996 312-90-60-90",
          user: users[16],
          approved: "approved"
      },
      {
          title: "Малыш",
          description: "Семейная клиника",
          address: "г.Бишкек, ул. Линейная, 67",
          phone: "+996 312-30–19–19",
          user: users[15],
          approved: "pending"
      },
      {
          title: "On Clinic",
          description: "Клиника",
          address: "г. Бишкек, ул. Веселая 46",
          phone: "+996 312-90-90-95",
          user: users[14],
          approved: "approved"
      }
  );

  const doctors = await DoctorProfile.create(
      {
          name: "Иван",
          surname: "Иванов",
          thirdname: "Иванович",
          passportId: "ID",
          passportNumber: 12132121,
          phone: "+996 555 555 555",
          license: "КР1212121",
          dateOfBirth: "12.12.12",
          yearsOfWork: 10,
          diploma: "КР1212121",
          clinic: clinics[1],
          schedule: "9.00 до 18.00",
          gender: "male",
          price: 500,
          doctorCategory: doctorCategories[0],
          speciality: "Стоматолог",
          user: users[1],
          workAddress: "Бишкек, Горького 3",
          approved: "pending"
      },
      {
          name: "Петр",
          surname: "Пертров",
          thirdname: "Петрович",
          passportId: "ID",
          passportNumber: 12132121,
          phone: "+996 555 555 555",
          license: "КР1212121",
          dateOfBirth: "12.12.12",
          yearsOfWork: 10,
          diploma: "КР1212121",
          clinic: clinics[1],
          schedule: "9.00 до 18.00",
          gender: "male",
          price: 600,
          doctorCategory: doctorCategories[0],
          speciality: "Стоматолог",
          user: users[2],
          workAddress: "Бишкек, Горького 3",
          approved: "approved"
      },
      {
          name: "Ася",
          surname: "Василиьева",
          thirdname: "Васильевна",
          passportId: "ID",
          passportNumber: 12132121,
          phone: "+996 555 555 555",
          license: "КР1212121",
          dateOfBirth: "12.12.12",
          yearsOfWork: 10,
          diploma: "КР1212121",
          clinic: clinics[1],
          schedule: "9.00 до 18.00",
          gender: "female",
          price: 700,
          doctorCategory: doctorCategories[0],
          speciality: "Стоматолог",
          user: users[3],
          workAddress: "Бишкек, Горького 3",
          approved: "approved"
      },
      {
          name: "Дмитрий",
          surname: "Дмитриев",
          thirdname: "Дмитриевич",
          passportId: "ID",
          passportNumber: 12132121,
          phone: "+996 555 555 555",
          license: "КР1212121",
          dateOfBirth: "12.12.12",
          yearsOfWork: 10,
          diploma: "КР1212121",
          clinic: clinics[1],
          schedule: "9.00 до 18.00",
          gender: "male",
          price: 500,
          doctorCategory: doctorCategories[0],
          speciality: "Стоматолог",
          user: users[4],
          workAddress: "Бишкек, Горького 3",
          approved: "pending"
      },
      {
          name: "Александр",
          surname: "Козлов",
          thirdname: "Алексеевич",
          passportId: "ID",
          passportNumber: 12132121,
          phone: "+996 555 555 555",
          license: "КР1212121",
          dateOfBirth: "12.12.12",
          yearsOfWork: 10,
          diploma: "КР1212121",
          clinic: clinics[1],
          schedule: "9.00 до 18.00",
          gender: "male",
          price: 500,
          doctorCategory: doctorCategories[0],
          speciality: "Стоматолог",
          user: users[5],
          workAddress: "Бишкек, Горького 3",
          approved: "pending"
      },
      {
          name: "Никита",
          surname: "Петров",
          thirdname: "Иванович",
          passportId: "ID",
          passportNumber: 12132121,
          phone: "+996 555 555 555",
          license: "КР1212121",
          dateOfBirth: "12.12.12",
          yearsOfWork: 10,
          diploma: "КР1212121",
          clinic: clinics[1],
          schedule: "9.00 до 18.00",
          gender: "male",
          price: 500,
          doctorCategory: doctorCategories[0],
          speciality: "Стоматолог",
          user: users[6],
          workAddress: "Бишкек, Горького 3",
          approved: "approved"
      },
  );

  const patients = await PatientProfile.create(
      {
          name: "Василий",
          surname: "Петров",
          thirdname: "Искакович",
          dateOfBirth: "12.12.12",
          gender: "male",
          passportId: "ID",
          passportNumber: 12132121,
          phone: "+996 555 555 555",
          workAddress: "ТЭЦ КР",
          residence: "ул. Ауэзова 50/2",
          user: users[7]
      },
      {
          name: "Василий",
          surname: "Петров",
          thirdname: "Искакович",
          dateOfBirth: "12.12.12",
          gender: "male",
          passportId: "ID",
          passportNumber: 12132121,
          phone: "+996 555 555 555",
          workAddress: "ТЭЦ КР",
          residence: "ул. Ауэзова 50/2",
          user: users[0]
      }
  );


  const records = await Record.create(
      {
          date: "12.07.2019",
          time: "15:00",
          patientConfirm: true,
          doctorId: doctors[1],
          patientId: patients[0],
          complaint: "Зуб болит"
      },
      {
          date: "28.06.2019",
          time: "16:30",
          patientConfirm: true,
          doctorConfirm: true,
          doctorId: doctors[2],
          patientId: patients[0],
          status: "approved",
          complaint: "Зуб болит"
      },
      {
          date: "11.07.2019",
          time: "09:30",
          patientConfirm: false,
          doctorConfirm: true,
          doctorId: doctors[2],
          patientId: patients[0],
          status: "rescheduled",
          complaint: "Зуб болит"
      }
  );

  const news = await News.create(
      {
          title: "Новость",
          description: "Описание Новости",
          image: "putin.jpeg",
          user: users[10],
      },
      {
          title: "Новость",
          description: "Описание Новости",
          image: "putin.jpeg",
          user: users[10],
      },
      {
          title: "Новость",
          description: "Описание Новости",
          image: "putin.jpeg",
          user: users[10],
      }
  );

  const comments = await Comment.create(
      {
          description: "Описание Новости",
          news: news[0],
          user: users[10],
      },
      {
          description: "Описание Новости",
          news: news[1],
          user: users[10],
      },
      {
          description: "Описание Новости",
          news: news[2],
          user: users[10],
      }
  );*/


  await connection.close();
};


run().catch(error => {
  console.log('Something went wrong', error);
});
