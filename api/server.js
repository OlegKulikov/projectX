import express from 'express';
import mongoose from 'mongoose';
import config from './config';
import cors from 'cors';

import users from './routes/users';
import doctorCategories from './routes/doctorCategories';
import doctorProfiles from './routes/doctorProfiles';
import patientProfiles from './routes/patientProfiles';
// const profiles = require('./routes/profiles');
// const clinicProfiles = require('./routes/clinicProfiles');
// const shopProfiles = require('./routes/shopProfiles');
// const admin = require('./routes/admin');
// const news = require('./routes/news');
// const comment = require('./routes/comment');
// const record = require('./routes/records');
// const notifications = require('./routes/notifications');
// const User = require('./models/User');
// const PatientProfile = require('./models/PatientProfile');
// const DoctorProfile = require('./models/DoctorProfile');
// const Record = require('./models/Record');
// const medicalCards = require('./routes/medicalCards');
// const questionnaire = require('./routes/questionnaire');


const app = express();
const port = process.env.NODE_ENV === 'test' ? 8010 : 8008;

app.use(express.json());

app.use(cors());
app.use(express.static('public'));

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {

  app.use('/users', users);
  app.use('/doctorProfiles', doctorProfiles);
  app.use('/doctorCategories', doctorCategories);
  app.use('/patientProfiles', patientProfiles);
  // app.use('/profiles', profiles);
  // app.use('/clinicProfiles', clinicProfiles);
  // app.use('/shopProfiles', shopProfiles);
  // app.use('/admin', admin);
  // app.use('/news', news);
  // app.use('/comment', comment);
  // app.use('/records', record);
  // app.use('/medical_cards', medicalCards);
  // app.use('/questionnaire', questionnaire);
  // app.use('/notifications', notifications);

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });

});
