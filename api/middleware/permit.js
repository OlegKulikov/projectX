const permit = (...roles) => {
  return (req, res, next) => {
    const {user} = req;
    if (!user) {
      return res.status(401).send({message: 'Unauthenticated'});
    }

    if(!roles.includes(...user.permissions)){
      return res.status(403).send({message: 'Unauthorized'});
    }

    next();
  }
};

module.exports = permit;