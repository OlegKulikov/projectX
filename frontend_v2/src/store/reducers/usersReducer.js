import {
  ASK_EMAIL_CODE_FAILURE,
  FETCH_USER_PROFILE_SUCCESS,
  LOGIN_USER_FAILURE,
  LOGIN_USER_REQUEST,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER_SUCCESS,
  REGISTER_USER_FAILURE,
  REGISTER_USER_REQUEST,
  REGISTER_USER_SUCCESS,
  RESET_PASSWORD_FAILURE,
  RESET_PASSWORD_SUCCESS,
} from "../actions/usersActions";

const initialState = {
  registerError: null,
  loginError: null,
  user: null,
  selectGroupLoading: false,
  resetPasswordError: null,
  confirmEmailMessage: '',
  accountMessage: null,
  isReferralIdValid: false,
  availableCountries: [],
  kycRegistrationError: {},
  loginRequestSending: false,
  registerRequestSending: false,
  resetPasswordRequestSending: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_USER_REQUEST:
      return {...state, registerRequestSending: true};
    case REGISTER_USER_SUCCESS:
      return {...state, registerError: null, user: action.user, registerRequestSending: false};
    case REGISTER_USER_FAILURE:
      return {...state, registerError: action.error, registerRequestSending: false};
    case LOGIN_USER_REQUEST:
      return {...state, loginRequestSending: true};
    case LOGIN_USER_SUCCESS:
      return {...state, user: action.user, loginError: null, loginRequestSending: false};
    case LOGIN_USER_FAILURE:
      return {...state, loginError: action.error, loginRequestSending: false};
    case LOGOUT_USER_SUCCESS:
      return {...state, user: null};
    case RESET_PASSWORD_SUCCESS:
      return {...state, user: action.user, resetPasswordError: null};
    case RESET_PASSWORD_FAILURE:
      return {...state, resetPasswordError: action.error};
    case ASK_EMAIL_CODE_FAILURE:
      return {...state, resetPasswordError: action.error};
    case FETCH_USER_PROFILE_SUCCESS:
      return {...state, user: action.user};
    default:
      return state;
  }
};

export default reducer;
