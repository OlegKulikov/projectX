import axios from '../axios-api';
import {forceLogoutUser} from "./actions/usersActions";
import config from "../config";

const configureInterceptors = store => {

  axios.interceptors.request.use(req => {
    try {
        const lang = localStorage.getItem("i18nextLng");
        if (store.getState().users.user) {
            req.headers['Authorization'] = 'Bearer ' + store.getState().users.user.token;
        }
        if (lang) {
            req.headers['Accept-Language'] = lang;
        }
    } catch (e) {
      // do nothing
    }

    return req;
  });

  // axios.interceptors.response.use(res => res,
  //   error => {
  //       if (error.response && error.response.status === 401 && error.response.config.url !== `${config.apiUrl}users/sessions`) {
  //         // return store.dispatch(forceLogoutUser());
  //     } else if (!error.response) {
  //       error.response = {data: {global: 'No connection to server'}};
  //       return Promise.reject(error);
  //     } else {
  //       return Promise.reject(error);
  //     }
  //   });
};

export default configureInterceptors;
