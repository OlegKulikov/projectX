export const REGISTER_USER_REQUEST = 'REGISTER_USER_REQUEST';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const registerUserRequest = userData => ({type: REGISTER_USER_REQUEST, userData});
export const registerUserSuccess = user => ({type: REGISTER_USER_SUCCESS, user});
export const registerUserFailure = error => ({type: REGISTER_USER_FAILURE, error});

export const LOGIN_USER_REQUEST = 'LOGIN_USER_REQUEST';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const loginUserRequest = (userData, params) => ({type: LOGIN_USER_REQUEST, userData, params});
export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});

export const LOGOUT_USER_REQUEST = 'LOGOUT_USER_REQUEST';
export const LOGOUT_USER_SUCCESS = 'LOGOUT_USER_SUCCESS';
export const FORCE_LOGOUT_USER = 'FORCE_LOGOUT_USER';

export const logoutUserRequest = () => ({type: LOGOUT_USER_REQUEST});
export const logoutUserSuccess = () => ({type: LOGOUT_USER_SUCCESS});
export const forceLogoutUser = () => ({type: FORCE_LOGOUT_USER});

export const ASK_EMAIL_CODE_REQUEST = 'ASK_EMAIL_CODE_REQUEST';
export const ASK_EMAIL_CODE_SUCCESS = 'ASK_EMAIL_CODE_SUCCESS';
export const ASK_EMAIL_CODE_FAILURE = 'ASK_EMAIL_CODE_FAILURE';

export const askEmailCodeRequest = userData => ({type: ASK_EMAIL_CODE_REQUEST, userData});
export const askEmailCodeSuccess = () => ({type: ASK_EMAIL_CODE_SUCCESS});
export const askEmailCodeFailure = error => ({type: ASK_EMAIL_CODE_FAILURE, error});

export const RESET_PASSWORD_REQUEST = 'VERIFY_CODE_REQUEST';
export const RESET_PASSWORD_SUCCESS = 'VERIFY_CODE_SUCCESS';
export const RESET_PASSWORD_FAILURE = 'VERIFY_CODE_FAILURE';

export const resetPasswordRequest = userData => ({type: RESET_PASSWORD_REQUEST, userData});
export const resetPasswordSuccess = user => ({type: RESET_PASSWORD_SUCCESS, user});
export const resetPasswordFailure = error => ({type: RESET_PASSWORD_FAILURE, error});

export const FETCH_USER_PROFILE_REQUEST = 'FETCH_USER_PROFILE_REQUEST';
export const FETCH_USER_PROFILE_SUCCESS = 'FETCH_USER_PROFILE_SUCCESS';
export const FETCH_USER_PROFILE_FAILURE = 'FETCH_USER_PROFILE_FAILURE';

export const fetchUserProfileRequest = () => ({type: FETCH_USER_PROFILE_REQUEST});
export const fetchUserProfileSuccess = user => ({type: FETCH_USER_PROFILE_SUCCESS, user});
export const fetchUserProfileFailure = error => ({type: FETCH_USER_PROFILE_FAILURE, error});