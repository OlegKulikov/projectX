import {put, takeEvery, takeLatest} from 'redux-saga/effects';
import {NotificationManager} from 'react-notifications';
import {
  ASK_EMAIL_CODE_REQUEST,
  askEmailCodeFailure,
  askEmailCodeSuccess,
  FETCH_USER_PROFILE_REQUEST,
  fetchUserProfileFailure,
  fetchUserProfileSuccess,
  FORCE_LOGOUT_USER,
  LOGIN_USER_REQUEST,
  loginUserFailure,
  loginUserSuccess,
  LOGOUT_USER_REQUEST,
  logoutUserSuccess,
  REGISTER_USER_REQUEST,
  registerUserFailure,
  registerUserSuccess,
  RESET_PASSWORD_REQUEST,
  resetPasswordFailure,
  resetPasswordSuccess,
} from "../actions/usersActions";
import {push, replace} from "connected-react-router";
import axios from '../../axios-api';

function* registerUserSaga({userData, params}) {
  try {
    const response = yield axios.post('/users/sign', userData, {params});
    yield put(registerUserSuccess(response.data.user));
    yield put(replace('/account/message'));
    yield NotificationManager.success(response.data.message);
  } catch (error) {
    yield put(registerUserFailure(error.response.data));
  }
}

function* loginUser({userData}) {
  try {
    const response = yield axios.post('/users/sessions', userData);
    yield put(loginUserSuccess(response.data.user));
    yield put(replace('/'));
    NotificationManager.success(  response.data.message);
  } catch (error) {
      yield put(loginUserFailure(error.response.data));
      NotificationManager.warning(error.response.data.error);
  }
}

function* askEmailCode({userData}) {
    try {
        yield axios.post("/api/v1/password/forgot", userData);
        yield put(askEmailCodeSuccess());
        yield put(replace('/account/reset-password/success'));
    } catch (error) {
        yield put(askEmailCodeFailure(error.response.data));
        yield put(push('/account/reset-password/error'));
    }
}

function* logoutUser() {
  try {
    yield put(logoutUserSuccess());
  } catch (error) {
    NotificationManager.error('Could not logout!');
  } finally {
    NotificationManager.success('Logged out successfully');
    yield put(replace('/login'));
  }
}

function* forceLogoutUser() {
  yield put(logoutUserSuccess());
  NotificationManager.warning('Not authenticated, please log in again');
  yield put(replace('/login'));
}

function* changePassword({userData}) {
  try {
    const response = yield axios.put("/api/v1/password/reset", userData);

    yield put(resetPasswordSuccess(response.data.user));
    NotificationManager.success('Password change successful');
    yield put(replace('/'))
  } catch (error) {
    yield put(resetPasswordFailure(error.response.data))
  }
}

function* updateUserProfile() {
    try {
        const response = yield axios.get('/api/v1/profile/');
        yield put(fetchUserProfileSuccess(response.data));
    } catch (e) {
        yield put(fetchUserProfileFailure(e));
    }
}

export default [
  takeEvery(LOGIN_USER_REQUEST, loginUser),
  takeEvery(REGISTER_USER_REQUEST, registerUserSaga),
  takeEvery(LOGOUT_USER_REQUEST, logoutUser),
  takeEvery(ASK_EMAIL_CODE_REQUEST, askEmailCode),
  takeEvery(RESET_PASSWORD_REQUEST, changePassword),
  takeEvery(FETCH_USER_PROFILE_REQUEST, updateUserProfile),
  takeLatest(FORCE_LOGOUT_USER, forceLogoutUser),
];
