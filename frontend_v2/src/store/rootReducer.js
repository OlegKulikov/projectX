import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';

import usersReducer from './reducers/usersReducer';

export default (history) => {
  return combineReducers({
    router: connectRouter(history),
    users: usersReducer,
  });
};
