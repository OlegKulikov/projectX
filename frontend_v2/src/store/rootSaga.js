import {all} from 'redux-saga/effects';
import usersSagas from './sagas/userSagas';

export default function* rootSaga() {
  yield all([
    ...usersSagas,
  ]);
}
