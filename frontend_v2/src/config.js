export const PRODUCTION_ENV = 'prod';
const config = {
  env: 'dev',
  apiUrl: 'http://localhost:8008/',
};

export default config;
