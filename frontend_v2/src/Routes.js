import React from "react";
import {Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {ProtectedRoute} from "./common/ProtectedRoute";
import withTitle from "./common/withTitle";
import Index from "./views/Index";
import NucleoIcons from "./views/NucleoIcons";
import LandingPage from "./views/examples/LandingPage";
import ProfilePage from "./views/ProfilePage";
import LoginPage from "./views/LoginPage";
import {Redirect, Route} from "react-router";
import RegisterPage from "./views/RegisterPage";

export const pathOfRoutes = {
  root: '/',
  profile: "/profile-page",
  login: "/login-page",
  register: "/create-account",
  doctors: "/doctors",
};

const Routes = ({user}) => (
  <Switch>
    <ProtectedRoute isAllowed={!user} path={pathOfRoutes.login} redirectTo={pathOfRoutes.root} component={withTitle({component: LoginPage, title: 'Login'})} />
    <ProtectedRoute isAllowed={!user} path={pathOfRoutes.register} redirectTo={pathOfRoutes.root} component={withTitle({component: RegisterPage, title: 'Register'})} />
    <Route
      path={pathOfRoutes.doctors}
      render={() => <LandingPage/>}
    />
    <Route
      path="/nucleo-icons"
      render={props => <NucleoIcons {...props} />}
    />
    <Route
      path="/landing-page"
      render={props => <LandingPage {...props} />}
    />
    <ProtectedRoute isAllowed={!!user}
                    path={pathOfRoutes.profile}
                    user={user}
                    render={props => <ProfilePage user={user} {...props} />}
    />
    <Route exact path={pathOfRoutes.root} render={props => <Index {...props} />} />
    <Redirect to={pathOfRoutes.root} />
  </Switch>
);


const mapStateToProps = state => ({
  user: state.users.user
});

export default withRouter(connect(mapStateToProps)(Routes));
