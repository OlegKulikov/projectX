import React from 'react';
import ReactDOM from 'react-dom';
import {ConnectedRouter} from 'connected-react-router';
import {Provider} from "react-redux";
import {I18nextProvider} from "react-i18next";
import {NotificationContainer} from "react-notifications";
import 'react-notifications/lib/notifications.css';

import * as serviceWorker from './serviceWorker';
import store, {history} from "./store/configureStore";
import App from './App';
import i18n from './i18n';
import "./assets/css/bootstrap.min.css";
import "./assets/scss/now-ui-kit.scss";
import "./assets/demo/demo.css";
import "./assets/demo/nucleo-icons-page-styles.css";

const app = (
    <I18nextProvider i18n={i18n}>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <App/>
                <NotificationContainer/>
            </ConnectedRouter>
        </Provider>
    </I18nextProvider>
);
ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
