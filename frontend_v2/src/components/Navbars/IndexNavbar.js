import React from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
// reactstrap components
import {
  Button,
  Collapse,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  UncontrolledTooltip
} from "reactstrap";
import {connect} from "react-redux";
import ReactLinkAdapter from "../../common/ReactLinkAdapter";
import {createBrowserHistory} from "history";
import {logoutUserRequest} from "../../store/actions/usersActions";
import {pathOfRoutes} from "../../Routes";
import config from "../../config";

function IndexNavbar({user, ...props}) {
  const [navbarColor, setNavbarColor] = React.useState("navbar-transparent");
  const [collapseOpen, setCollapseOpen] = React.useState(false);
  const history = useHistory();

  React.useEffect(() => {
    const updateNavbarColor = () => {
      if (
        document.documentElement.scrollTop > 399 ||
        document.body.scrollTop > 399
      ) {
        setNavbarColor("");
      } else if (
        document.documentElement.scrollTop < 400 ||
        document.body.scrollTop < 400
      ) {
        setNavbarColor("navbar-transparent");
      }
    };
    window.addEventListener("scroll", updateNavbarColor);
    return function cleanup() {
      window.removeEventListener("scroll", updateNavbarColor);
    };
  });
  return (
    <>
      {collapseOpen ? (
        <div
          id="bodyClick"
          onClick={() => {
            document.documentElement.classList.toggle("nav-open");
            setCollapseOpen(false);
          }}
        />
      ) : null}
      <Navbar className={"fixed-top " + navbarColor} expand="lg" color="info">
        <Container>
          <div className="navbar-translate">
            <Link to='/'>
              <NavbarBrand
                id="navbar-brand"
              >
                MED-X
              </NavbarBrand>
            </Link>
            <UncontrolledTooltip target="#navbar-brand">
              Best doctors social groupI
            </UncontrolledTooltip>
            <button
              className="navbar-toggler navbar-toggler"
              onClick={() => {
                document.documentElement.classList.toggle("nav-open");
                setCollapseOpen(!collapseOpen);
              }}
              aria-expanded={collapseOpen}
              type="button"
            >
              <span className="navbar-toggler-bar top-bar"></span>
              <span className="navbar-toggler-bar middle-bar"></span>
              <span className="navbar-toggler-bar bottom-bar"></span>
            </button>
          </div>
          <Collapse
            className="justify-content-between"
            isOpen={collapseOpen}
            navbar
          >
            <Nav navbar>
              <NavItem>
                <Link to={pathOfRoutes.doctors}>
                  <NavLink>
                    <p>Doctors</p>
                  </NavLink>
                </Link>
              </NavItem>
              <NavItem>
                <Link>
                  <NavLink>
                    <p>Shop</p>
                  </NavLink>
                </Link>
              </NavItem>
              <NavItem>
                <NavLink>
                  <p>About</p>
                </NavLink>
              </NavItem>
            </Nav>
            <Nav navbar>
              {/*<UncontrolledDropdown nav>*/}
              {/*  <DropdownToggle*/}
              {/*    caret*/}
              {/*    color="default"*/}
              {/*    href="#pablo"*/}
              {/*    nav*/}
              {/*    onClick={e => e.preventDefault()}*/}
              {/*  >*/}
              {/*    <i className="now-ui-icons design_app mr-1"></i>*/}
              {/*    <p>Components</p>*/}
              {/*  </DropdownToggle>*/}
              {/*  <DropdownMenu>*/}
              {/*    <DropdownItem to="/index" tag={Link}>*/}
              {/*      <i className="now-ui-icons business_chart-pie-36 mr-1"></i>*/}
              {/*      All components*/}
              {/*    </DropdownItem>*/}
              {/*    <DropdownItem*/}
              {/*      href="https://demos.creative-tim.com/now-ui-kit-react/#/documentation/introduction?ref=nukr-index-navbar"*/}
              {/*      target="_blank"*/}
              {/*    >*/}
              {/*      <i className="now-ui-icons design_bullet-list-67 mr-1"></i>*/}
              {/*      Documentation*/}
              {/*    </DropdownItem>*/}
              {/*  </DropdownMenu>*/}
              {/*</UncontrolledDropdown>*/}
              {!user ? <NavItem>
                <Link to='/login-page'>
                  <Button
                    className="nav-link btn-neutral"
                    color="info"
                  >
                    <i className="now-ui-icons arrows-1_share-66 mr-1"/>
                    <p>Sign in</p>
                  </Button>
                </Link>
              </NavItem> : <>
                <UncontrolledDropdown nav>
                  <DropdownToggle
                    caret
                    color="default"
                    nav
                  >
                    {user.email}
                      <img
                        style={{width: '25px', height: '25px'}}
                        className='ml-2 rounded-circle img-raised'
                        src={user.avatar ? config.apiUrl + user.avatar : require('../../assets/default.png')}/>
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem
                      onClick={() => history.push(pathOfRoutes.profile)}
                    >
                      Profile
                    </DropdownItem>
                    <div className="divider border border-bottom-0"/>
                    <DropdownItem
                      onClick={() => props.dispatch(logoutUserRequest())}
                    >
                      Logout
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </>}
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </>
  );
}

const mapStateToProps = state => ({
  user: state.users.user
});

export default connect(mapStateToProps)(IndexNavbar);
