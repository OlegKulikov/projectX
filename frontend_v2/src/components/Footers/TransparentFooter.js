/*eslint-disable*/
import React from "react";

// reactstrap components
import { Container } from "reactstrap";
import {Link} from "react-router-dom";

function TransparentFooter() {
  return (
    <footer className="footer">
      <Container>
        <nav>
          <ul>
            <li>
              <Link
                to='/'
              >
                MED-X
              </Link>
            </li>
          </ul>
        </nav>
        <div className="copyright" id="copyright">
          © {new Date().getFullYear()}, Designed by{" "}
          <a
            href="https://www.invisionapp.com?ref=nukr-transparent-footer"
            target="_blank"
          >
            Invision
          </a>
          . Coded by{" "}
          <a
            href="https://vk.com/meow_k073"
            target="_blank"
          >
            Laptev Vladislav
          </a>
          .
        </div>
      </Container>
    </footer>
  );
}

export default TransparentFooter;
