import {connect} from 'react-redux';

/**
 * @return {null}
 */
function ProtectedGroupComponent({permissions, ...props}) {
  function checkPermission(perms) {
    if (props.userPermissions && perms) {
      return perms.some(perm=> props.userPermissions.includes(perm));
    }
    return false;
  }

  return checkPermission(permissions) ? props.children : null;
}

const mapStateToProps = state => ({
  userPermissions: state.users.user.user.roles || [],
});

export default connect(mapStateToProps)(ProtectedGroupComponent);
