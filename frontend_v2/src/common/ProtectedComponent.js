import {connect} from 'react-redux';

/**
 * @return {null}
 */
function ProtectedComponent({permissions, ...props}) {
  function checkPermission(perms) {
    if (props.userPermissions && perms) {
      return perms.every(perm=> props.userPermissions.includes(perm));
    }
    return false;
  }

  return checkPermission(permissions) ? props.children : null;
}

const mapStateToProps = state => ({
  userPermissions: state.users.user.user.permissions || [],
});

export default connect(mapStateToProps)(ProtectedComponent);
