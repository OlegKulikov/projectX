import React from "react";
import Helmet from "react-helmet";
import {withTranslation} from "react-i18next";

const TitleComponent = ({title}) => {
    const defaultTitle = "MED-X";
    return (
        <Helmet>
            <title>{defaultTitle + (title ? " - " + title : "")}</title>
        </Helmet>
    )
};

const withTitle = ({ component: Component, title, ...props }) => {
    class Title extends React.Component {
        render() {
            const {t} = this.props;
            return (
                <React.Fragment>
                    <TitleComponent title={t(title)} />
                    <Component {...this.props} {...props} />
                </React.Fragment>
            );
        }
    }
    return withTranslation()(Title)
};

export default withTitle;