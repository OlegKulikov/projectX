import React from "react";
import {Redirect, Route} from "react-router-dom";

export const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => (
    isAllowed ? <Route {...props}/> : <Redirect to={redirectTo} />
);