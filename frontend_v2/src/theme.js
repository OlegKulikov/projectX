import { createMuiTheme } from "@material-ui/core";


export const theme = createMuiTheme({
    palette: {
        type: 'dark',
        // common: {
        //   black: '#fff',
        //     white: '#fff'
        // },
        primary: {
            main: '#188ec1',
            contrastText: '#fff',
        },
        error: {
            main: '#fd5d93',
        },
        secondary: {
            main: 'hsla(0,0%,100%,.8)',
            contrastText: "#303030"
        },
        // background: {
        //   default: '#009FE3',
        //     paper: '#fff'
        // },
        // color: {
        //     default: '#ffffff'
        // }
    },
    overrides: {
        // MuiFormControlLabel: {
        //   root: {
        //       color: '#fff',
        //   }
        // },
        MuiInputBase: {
            // root: {
            //     borderRadius: 3,
            //     borderWidth: 1,
            //     backgroundColor: '#33B2E9',
            //     color: 'white',
            //     '&::placeholder': {
            //         color: 'white'
            //     },
            //     "&:active": {
            //         outlineWidth: 0
            //     },
            //     '&$focused': {
            //       },
            // },
        //     focused: {},
        },
        // MuiButton: {
        //     containedPrimary: {
        //         backgroundColor: 'white',
        //         borderRadius: 30,
        //         border: '1px solid white',
        //         boxShadow: 'none',
        //         color: '#009FE3',
        //         "&:hover": {
        //             backgroundColor: 'transparent !important',
        //             color: 'white',
        //         }
        //     },
        //     contained: {
        //         "&:active": {
        //             boxShadow: 'none'
        //         }
        //     }
        // },
        // MuiCheckbox: {
        //     root: {
        //         color: 'white',
        //         "&$checked": {
        //             color: 'white',
        //             fill: 'white',
        //         }
        //     }
        // }
    },
});