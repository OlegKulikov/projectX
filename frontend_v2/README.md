## BeQ Frontend app

Prerequisites:
* NodeJS 10.16.3
* Yarn

Install and run in development mode:

```
$ yarn
$ yarn start
```

Running code linting
```
yarn lint
```
