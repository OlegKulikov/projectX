import { environment } from '../../environments/environment'

const base = (rest: string) => `${environment.apiUrl}${rest}`

class Paths {
}

export default Paths
