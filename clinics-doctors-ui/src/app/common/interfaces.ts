export type IClassType<T> = new () => T
