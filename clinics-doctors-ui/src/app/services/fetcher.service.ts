/*CORE*/
import { Injectable } from '@angular/core'
import { catchError, map } from 'rxjs/operators'
import { Observable } from 'rxjs/internal/Observable'
import { throwError } from 'rxjs/internal/observable/throwError'
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'
/*LIBS*/
import { JsonConvert } from 'json2typescript'
/*INTERFACES*/
import { IClassType } from '../common/interfaces'

@Injectable()
export class Fetcher {

  constructor(private _http: HttpClient) {
  }

  fetchData<T>(method: string, url: string, body = {}, model: IClassType<T>, formData = false, withCredentials = true): Observable<T> {

    let headers = new HttpHeaders()
    const converter: JsonConvert = new JsonConvert()

    headers =
      formData ?
        headers.append('Content-Type', 'multipart/form-data') : headers.append('Content-Type', 'application/json')

    const options = {withCredentials, headers}

    method === 'GET' ? (options['params'] = body) : (options['body'] = formData ? body : JSON.stringify(body))

    return this._http.request(method, url, options).pipe(
      map((response: T) => converter.deserializeObject(response, model)),
      catchError((err: HttpErrorResponse) => throwError(err.error))
    )
  }
}
