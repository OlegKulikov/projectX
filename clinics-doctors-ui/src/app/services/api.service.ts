/*CORE*/
import { Injectable } from '@angular/core'
import { Fetcher } from './fetcher.service'
import { Observable } from 'rxjs/internal/Observable'
/*INTERFACES*/
import { IClassType } from '../common/interfaces'

@Injectable()
export class ApiService {

  constructor(private _fetcher: Fetcher) {
  }

  put<T>(url: string, body = {}, model: IClassType<T>, withCredentials): Observable<T> {
    return this._fetcher.fetchData<T>('PUT', url, body, model, withCredentials)
  }

  get<T>(url: string, body = {}, model: IClassType<T>, withCredentials): Observable<T> {
    return this._fetcher.fetchData<T>('GET', url, body, model, withCredentials)
  }

  delete<T>(url: string, body = {}, model: IClassType<T>, withCredentials): Observable<T> {
    return this._fetcher.fetchData<T>('DELETE', url, body, model, withCredentials)
  }

  post<T>(url: string, body = {}, model: IClassType<T>, formData, withCredentials): Observable<T> {
    return this._fetcher.fetchData<T>('POST', url, body, model, formData, withCredentials)
  }
}
