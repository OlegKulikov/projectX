/*CORE*/
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Component } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { DomSanitizer, SafeHtml } from '@angular/platform-browser'

@Component({
  selector: 'app-root',
  template: `
    <div [innerHtml]="svgSprite$ | async" [style.display]='"none"'></div>
    <h1>!!! Project X WORKS !!!</h1>
  `,
})
export class AppComponent {

  svgSprite$: Observable<SafeHtml>

  constructor(
    private _http: HttpClient,
    private _sanitizer: DomSanitizer,
  ) {
    this._injectSVG()
  }

  private _injectSVG() {
    const headers = new HttpHeaders({
      'Content-Type': 'text/plain'
    })
    this.svgSprite$ = this._http.get('assets/icons/svg-icons/sprites/icons-sprite.svg', {headers, responseType: 'text'})
      .pipe(map(sprite => this._sanitizer.bypassSecurityTrustHtml(sprite)))
  }
}
